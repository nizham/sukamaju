import React, { useRef, useState } from "react";

import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import { Toast } from "primereact/toast";

import "./Pages.css";

import { create, del, readAll, update } from "../services/PelangganSrv";

export function Pelanggan() {
  const [displayBasic, setDisplayBasic] = useState(false);
  const [correction, setCorrection] = useState(false);
  const [dataPelanggan, setDataPelanggan] = useState([]);
  const [selected, setSelected] = useState(null);
  const toast = useRef(null);

  const [id, setId] = useState(0);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [telp, setTelp] = useState("");

  function refresh() {
    var dataBox = [];
    readAll()
      .then((result) => {
        for (let i = 0; i < result.data.length; i++) {
          var extracted = {
            no: i + 1,
            id: result.data[i].id,
            nama: result.data[i].nama,
            alamat: result.data[i].alamat,
            telp: result.data[i].telp,
          };
          dataBox.push(extracted);
        }
        setDataPelanggan(dataBox);
      })
      .catch((reason) => {
        console.log(reason);
      });
  }

  function save() {
    let cust = { nama: nama, alamat: alamat, telp: telp };
    create(cust);
    setDisplayBasic(false);
    clear();
    toast.current.show({ severity: "info", summary: "Pelanggan", detail: `Berhasil simpan ${cust.nama}`, life: 3000 });
  }

  function update_selected() {
    let cust = {
      id: id,
      nama: nama,
      alamat: alamat,
      telp: telp,
    };
    update(cust);
    clear();
    toast.current.show({ severity: "info", summary: "Pelanggan", detail: "Berhasil update pelanggan", life: 3000 });
    setDisplayBasic(false);
  }

  function delete_selected() {
    del(id);
    clear();
    toast.current.show({ severity: "info", summary: "Pelanggan", detail: "Berhasil hapus pelanggan", life: 3000 });
    setDisplayBasic(false);
  }

  function clear() {
    setId(0);
    setNama("");
    setAlamat("");
    setTelp("");
  }

  const showForm = () => {
    if (displayBasic === false) {
      setDisplayBasic(true);
    } else {
      setDisplayBasic(false);
      clear();
    }
  };

  const onRowSelect = (event) => {
    setCorrection(true);
    setId(event.data.id);
    setNama(event.data.nama);
    setAlamat(event.data.alamat);
    setTelp(event.data.telp);
    setDisplayBasic(true);
  };

  const header = (
    <div className="table-header">
      <Button icon="pi pi-plus" onClick={() => showForm()} label="Tambah" className="p-button-sm p-button-outlined" />
      <Button onClick={refresh} className="p-button-sm p-button-success p-button-outlined" icon="pi pi-refresh" title="reload" />
    </div>
  );

  const footer = (
    <div className="table-footer">
      <Button icon="pi pi-times" onClick={() => showForm()} label="Batal" className="p-button-sm p-button-outlined p-button-secondary" />
      <span hidden={correction}>
        <Button icon="pi pi-check" onClick={() => save()} label="Simpan" className="p-button-sm p-button-outlined p-button-success" />
      </span>
      <span hidden={!correction}>
        <Button icon="pi pi-trash" onClick={() => delete_selected()} label="Hapus" className="p-button-sm p-button-outlined p-button-danger" />
        <Button icon="pi pi-check" onClick={() => update_selected()} label="Update" className="p-button-sm p-button-outlined p-button-success" />
      </span>
    </div>
  );

  return (
    <div>
      <Toast ref={toast} />
      <h2>Pelanggan</h2>
      <DataTable value={dataPelanggan} header={header} selectionMode="single" selection={selected} onSelectionChange={(e) => setSelected(e.value)} dataKey="id" onRowSelect={(e) => onRowSelect(e)}>
        <Column field="id" header="ID" className="hide-col"></Column>
        <Column field="no" header="No"></Column>
        <Column field="nama" header="Nama"></Column>
        <Column field="alamat" header="Alamat"></Column>
        <Column field="telp" header="No. Telp"></Column>
      </DataTable>

      <Dialog header="Pelanggan" footer={footer} visible={displayBasic} style={{ width: "40vw" }} onHide={() => showForm()}>
        <div className="formgrid">
          <div className="field">
            <label htmlFor="nama">Nama</label>
            <InputText value={nama} onChange={(e) => setNama(e.target.value)} id="nama" className="w-full" autoFocus />
          </div>
          <div className="field">
            <label htmlFor="alamat">Alamat</label>
            <InputTextarea value={alamat} onChange={(e) => setAlamat(e.target.value)} id="alamat" autoResize className="w-full" />
          </div>
          <div className="field">
            <label htmlFor="telp">Telp.</label>
            <InputText value={telp} onChange={(e) => setTelp(e.target.value)} id="telp" className="w-full" />
          </div>
        </div>
      </Dialog>
    </div>
  );
}
