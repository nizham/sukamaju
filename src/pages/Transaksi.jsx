import React, { useRef, useState } from "react";

import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { InputNumber } from "primereact/inputnumber";
import { Calendar } from "primereact/calendar";
import { Dropdown } from "primereact/dropdown";

import { Toast } from "primereact/toast";

import "./Pages.css";

import { create, del, withCustomer, update } from "../services/TransaksiSrv";
import { readAll } from "../services/PelangganSrv";

export function Transaksi() {
  const [displayBasic, setDisplayBasic] = useState(false);
  const [correction, setCorrection] = useState(false);
  const [dataTrans, setDataTrans] = useState([]);
  const [selected, setSelected] = useState(null);
  const toast = useRef(null);

  const [id, setId] = useState(0);
  const [kode, setKode] = useState("");
  const [tanggal, setTanggal] = useState("");
  const [pelanggan, setPelanggan] = useState(0);
  const [total, setTotal] = useState(0);

  const [customer, setCustomer] = useState([]);
  const [selectedCust, setSelectedCust] = useState(0);

  function refresh() {
    var dataBox = [];
    withCustomer()
      .then((result) => {
        for (let i = 0; i < result.data.length; i++) {
          var extracted = {
            no: i + 1,
            id: result.data[i].id,
            kode: result.data[i].kode,
            tanggal: result.data[i].tanggal,
            pelanggan: result.data[i].pelanggan,
            nama: result.data[i].nama,
            total: result.data[i].total,
          };
          dataBox.push(extracted);
        }
        setDataTrans(dataBox);
      })
      .catch((reason) => {
        console.log(reason);
      });
  }

  function grabCustomer() {
    var dataBox = [];
    readAll()
      .then((result) => {
        for (let i = 0; i < result.data.length; i++) {
          var extracted = {
            label: result.data[i].nama,
            value: result.data[i].id,
          };
          dataBox.push(extracted);
        }
        setCustomer(dataBox);
      })
      .catch((reason) => {
        console.log(reason);
      });
  }

  function save() {
    let Transaksi = { kode: kode, tanggal: tanggal, pelanggan: pelanggan, total: total };
    create(Transaksi);
    setDisplayBasic(false);
    clear();
    toast.current.show({ severity: "info", summary: "Transaksi", detail: `Berhasil simpan ${Transaksi.kode}`, life: 3000 });
  }

  function update_selected() {
    let trans = {
      id: id,
      kode: kode,
      tanggal: tanggal,
      pelanggan: pelanggan,
      total: total,
    };
    update(trans);
    clear();
    toast.current.show({ severity: "info", summary: "Transaksi", detail: "Berhasil update transaksi", life: 3000 });
    setDisplayBasic(false);
  }

  function delete_selected() {
    del(id);
    clear();
    toast.current.show({ severity: "info", summary: "Transaksi", detail: "Berhasil hapus transaksi", life: 3000 });
    setDisplayBasic(false);
  }

  function clear() {
    setId(0);
    setKode("");
    setTanggal("");
    setPelanggan(0);
    setTotal(0);
    setCustomer([]);
  }

  const showForm = () => {
    if (displayBasic === false) {
      grabCustomer();
      setDisplayBasic(true);
    } else {
      setDisplayBasic(false);
      clear();
    }
  };

  const onRowSelect = (event) => {
    setCorrection(true);
    setId(event.data.id);
    setKode(event.data.kode);
    setTanggal(event.data.tanggal);
    setPelanggan(event.data.pelanggan);
    setTotal(event.data.total);
    setDisplayBasic(true);
  };

  const header = (
    <div className="table-header">
      <Button icon="pi pi-plus" onClick={() => showForm()} label="Tambah" className="p-button-sm p-button-outlined" />
      <Button onClick={refresh} className="p-button-sm p-button-success p-button-outlined" icon="pi pi-refresh" title="reload" />
    </div>
  );

  const footer = (
    <div className="table-footer">
      <Button icon="pi pi-times" onClick={() => showForm()} label="Batal" className="p-button-sm p-button-outlined p-button-secondary" />
      <span hidden={correction}>
        <Button icon="pi pi-check" onClick={() => save()} label="Simpan" className="p-button-sm p-button-outlined p-button-success" />
      </span>
      <span hidden={!correction}>
        <Button icon="pi pi-trash" onClick={() => delete_selected()} label="Hapus" className="p-button-sm p-button-outlined p-button-danger" />
        <Button icon="pi pi-check" onClick={() => update_selected()} label="Simpan" className="p-button-sm p-button-outlined p-button-success" />
      </span>
    </div>
  );

  return (
    <div>
      <Toast ref={toast} />
      <h2>Transaksi</h2>
      <DataTable value={dataTrans} header={header} selectionMode="single" selection={selected} onSelectionChange={(e) => setSelected(e.value)} dataKey="id" onRowSelect={(e) => onRowSelect(e)}>
        <Column header="ID" field="id" className="hide-col"></Column>
        <Column header="No" field="no"></Column>
        <Column header="Kode Transaksi" field="kode"></Column>
        <Column header="Tanggal" field="tanggal"></Column>
        <Column header="PelangganID" field="pelanggan" className="hide-col"></Column>
        <Column header="Pelanggan" field="nama"></Column>
        <Column header="Total" field="total"></Column>
      </DataTable>

      <Dialog header="Transaksi" footer={footer} visible={displayBasic} style={{ width: "40vw" }} onHide={() => showForm()}>
        <div className="formgrid">
          <div className="field">
            <label htmlFor="kode">Kode</label>
            <InputText value={kode} onChange={(e) => setKode(e.target.value)} id="kode" className="w-full" autoFocus />
          </div>
          <div className="field">
            <label htmlFor="tanggal">Tanggal</label>
            <Calendar value={tanggal} onChange={(e) => setTanggal(e.value)} id="tanggal" className="w-full"></Calendar>
          </div>
          <div className="field">
            <label htmlFor="pelanggan">Pelanggan</label>
            <Dropdown value={selectedCust} options={customer} onChange={(e) => setSelectedCust(e.value)} placeholder="Pilih pelanggan" className="w-full" />
          </div>
          <div className="field">
            <label htmlFor="total">Total</label>
            <InputNumber value={total} onValueChange={(e) => setTotal(e.value)} id="total" className="w-full" />
          </div>
        </div>
        <div className="card">
          <DataTable>
            <Column header="No"></Column>
            <Column header="Nama Produk"></Column>
            <Column header="Harga Satuan"></Column>
            <Column header="Jumlah"></Column>
            <Column header="Sub Total"></Column>
          </DataTable>
        </div>
      </Dialog>
    </div>
  );
}
