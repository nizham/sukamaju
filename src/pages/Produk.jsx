import React, { useRef, useState } from "react";

import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import { InputNumber } from "primereact/inputnumber";
import { InputTextarea } from "primereact/inputtextarea";
import { Toast } from "primereact/toast";

import "./Pages.css";

import { create, del, readAll, update } from "../services/ProdukSrv";

export function Produk() {
  const [displayBasic, setDisplayBasic] = useState(false);
  const [correction, setCorrection] = useState(false);
  const [dataProduk, setDataProduk] = useState([]);
  const [selected, setSelected] = useState(null);
  const toast = useRef(null);

  const [id, setId] = useState(0);
  const [nama, setNama] = useState("");
  const [harga, setHarga] = useState(0);
  const [deskripsi, setDeskripsi] = useState("");

  function refresh() {
    var dataBox = [];
    readAll()
      .then((result) => {
        for (let i = 0; i < result.data.length; i++) {
          var extracted = {
            no: i + 1,
            id: result.data[i].id,
            nama: result.data[i].nama,
            harga: result.data[i].harga,
            deskripsi: result.data[i].deskripsi,
          };
          dataBox.push(extracted);
        }
        setDataProduk(dataBox);
      })
      .catch((reason) => {
        console.log(reason);
      });
  }

  function save() {
    let produk = { nama: nama, harga: harga, deskripsi: deskripsi };
    create(produk);
    setDisplayBasic(false);
    clear();
    toast.current.show({ severity: "info", summary: "Produk", detail: `Berhasil simpan ${produk.nama}`, life: 3000 });
  }

  function update_selected() {
    let prod = {
      id: id,
      nama: nama,
      harga: harga,
      deskripsi: deskripsi,
    };
    update(prod);
    clear();
    toast.current.show({ severity: "info", summary: "Produk", detail: "Berhasil update produk", life: 3000 });
    setDisplayBasic(false);
  }

  function delete_selected() {
    del(id);
    clear();
    toast.current.show({ severity: "info", summary: "Produk", detail: "Berhasil hapus produk", life: 3000 });
    setDisplayBasic(false);
  }

  function clear() {
    setId(0);
    setNama("");
    setHarga("");
    setDeskripsi("");
  }

  const showForm = () => {
    if (displayBasic === false) {
      setDisplayBasic(true);
    } else {
      setDisplayBasic(false);
      clear();
    }
  };

  const onRowSelect = (event) => {
    setCorrection(true);
    setId(event.data.id);
    setNama(event.data.nama);
    setHarga(event.data.harga);
    setDeskripsi(event.data.deskripsi);
    setDisplayBasic(true);
  };

  const header = (
    <div className="table-header">
      <Button icon="pi pi-plus" onClick={() => showForm()} label="Tambah" className="p-button-sm p-button-outlined" />
      <Button onClick={refresh} className="p-button-sm p-button-success p-button-outlined" icon="pi pi-refresh" title="reload" />
    </div>
  );

  const footer = (
    <div className="table-footer">
      <Button icon="pi pi-times" onClick={() => showForm()} label="Batal" className="p-button-sm p-button-outlined p-button-secondary" />
      <span hidden={correction}>
        <Button icon="pi pi-check" onClick={() => save()} label="Simpan" className="p-button-sm p-button-outlined p-button-success" />
      </span>
      <span hidden={!correction}>
        <Button icon="pi pi-trash" onClick={() => delete_selected()} label="Hapus" className="p-button-sm p-button-outlined p-button-danger" />
        <Button icon="pi pi-check" onClick={() => update_selected()} label="Update" className="p-button-sm p-button-outlined p-button-success" />
      </span>
    </div>
  );

  return (
    <div>
      <Toast ref={toast} />
      <h2>Produk</h2>
      <DataTable value={dataProduk} header={header} selectionMode="single" selection={selected} onSelectionChange={(e) => setSelected(e.value)} dataKey="id" onRowSelect={(e) => onRowSelect(e)}>
        <Column field="id" header="ID" className="hide-col"></Column>
        <Column field="no" header="No"></Column>
        <Column field="nama" header="Nama"></Column>
        <Column field="harga" header="Harga"></Column>
        <Column field="deskripsi" header="Deskripsi"></Column>
      </DataTable>

      <Dialog header="Produk" footer={footer} visible={displayBasic} style={{ width: "40vw" }} onHide={() => showForm()}>
        <div className="formgrid">
          <div className="field">
            <label htmlFor="nama">Nama</label>
            <InputText value={nama} onChange={(e) => setNama(e.target.value)} id="nama" className="w-full" autoFocus />
          </div>
          <div className="field">
            <label htmlFor="harga">Harga</label>
            <InputNumber value={harga} onValueChange={(e) => setHarga(e.value)} id="harga" className="w-full" />
          </div>
          <div className="field">
            <label htmlFor="deskripsi">Deskripsi.</label>
            <InputTextarea value={deskripsi} onChange={(e) => setDeskripsi(e.target.value)} id="deskripsi" autoResize className="w-full" />
          </div>
        </div>
      </Dialog>
    </div>
  );
}
