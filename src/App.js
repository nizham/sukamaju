import { Outlet } from "react-router-dom";

import "primereact/resources/themes/lara-light-indigo/theme.css"; //theme
import "primereact/resources/primereact.min.css"; //core css
import "primeicons/primeicons.css"; //icons
import "./App.css";

import { Menubar } from "primereact/menubar";

export default function App() {
  const items = [
    {
      label: "Home",
      icon: "pi pi-fw pi-home",
      url: "/",
    },
    {
      label: "Pelanggan",
      icon: "pi pi-fw pi-user",
      url: "/customer",
    },
    {
      label: "Produk",
      icon: "pi pi-fw pi-file",
      url: "/product",
    },
    { label: "Transaksi", icon: "pi pi-fw pi-pencil", url: "/transaction" },
  ];

  return (
    <div>
      <div className="card">
        <Menubar model={items} />
      </div>
      <div className="card app">
        <Outlet />
      </div>
    </div>
  );
}
