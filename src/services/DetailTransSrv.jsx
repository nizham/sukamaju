import Axios from "axios";

export function create(data) {
  let response = "";
  Axios.post(process.env.SUKAMAJU_API_URL + "/detailtransaksi", data)
    .then((res) => {
      response = res;
    })
    .catch((reason) => {
      response = "Error: " + reason;
    });
  return response;
}

export function readAll() {
  let response = [];
  Axios.get(process.env.SUKAMAJU_API_URL + "/detailtransaksi")
    .then((data) => {
      response = data;
    })
    .catch((reason) => {
      response = "Error: " + reason;
    });
  return response;
}

export function read(id) {
  let response = [];
  Axios.get(process.env.SUKAMAJU_API_URL + "/detailtransaksi/" + id)
    .then((data) => {
      response = data;
    })
    .catch((reason) => {
      response = "Error: " + reason;
    });
  return response;
}

export function update(data) {
  let response = "";
  Axios.put(process.env.SUKAMAJU_API_URL + "/detailtransaksi", data)
    .then((res) => {
      response = res;
    })
    .catch((reason) => {
      response = "Error: " + reason;
    });
  return response;
}

export function del(id) {
  let response = "";
  Axios.delete(process.env.SUKAMAJU_API_URL + "/detailtransaksi/" + id)
    .then((res) => {
      response = res;
    })
    .catch((reason) => {
      response = "Error: " + reason;
    });
  return response;
}
