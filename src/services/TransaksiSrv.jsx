import Axios from "axios";

export function create(data) {
  let response = "";
  Axios.post("http://localhost:8080/transaksi", data)
    .then((res) => {
      response = res;
    })
    .catch((reason) => {
      response = "Error: " + reason;
    });
  return response;
}

export function readAll() {
  return Axios.get("http://localhost:8080/transaksi");
}

export function withCustomer() {
  return Axios.get("http://localhost:8080/transaksi/cname");
}

export function read(id) {
  let response = [];
  Axios.get("http://localhost:8080/transaksi/" + id)
    .then((data) => {
      response = data;
    })
    .catch((reason) => {
      response = "Error: " + reason;
    });
  return response;
}

export function update(data) {
  let response = "";
  Axios.put("http://localhost:8080/transaksi", data)
    .then((res) => {
      response = res;
    })
    .catch((reason) => {
      response = "Error: " + reason;
    });
  return response;
}

export function del(id) {
  let response = "";
  Axios.delete("http://localhost:8080/transaksi/" + id)
    .then((res) => {
      response = res;
    })
    .catch((reason) => {
      response = "Error: " + reason;
    });
  return response;
}
