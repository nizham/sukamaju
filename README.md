# Test Coach Software Fullstack Engineer

Bagian A dapat dijalankan di web browser secara langsung, sebab hanya menggunakan
HTML dan javascript. File nya adalah TestCoachSoftwareEngineer.html

## Cara menjalankan aplikasi

Persayaratan:

- nodejs versi 16.14.0
- npm versi 8.3.1

Setelah clone repository ini
Di dalam folder project, ketik perintah berikut untuk menjalankan aplikasi:

### `npm install`

lalu

### `npm start`

lalu buka aplikasi di web browser dengan alamat:
http://localhost:3000

Catatan:
Sebelum menjalan aplikasi front-end ini, sebaiknya menjalankan aplikasi back-end terlebih dahulu,
yaitu "sukamaju_api"
